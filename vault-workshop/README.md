# Welcome to HashiCorp Vault Workshop

[HashiCorp Vault](https://www.vaultproject.io/) is designed to be the single source of secrets where highly sensitive data can be encrypted and stored. Vault allows you to control access to the secrets using policies.  

[![Introduction to HashiCorp Vault](https://hashicorp-education.s3-us-west-2.amazonaws.com/ASWSGameDay/IntroVault.png)](https://youtu.be/VYfl-DpZ5wM)

# Workshop Diagram

![Workshop Diagram](docs/img/Vault_Workshop_Diagram.png)

## Workshop Scenario Introduction

Unicorn Pet Clinic Inc has just launched its intranet Web site to facilicate the registration of new pet pets, their owners, and visits to its franchise Veterinary clinics. Its intranet Web site is hosted on AWS EC2 with a AWS RDS MySQL database to store records of its customers. After an audit by a third-party cloud security organization, its CIO has approved the deployment of HashiCorp Vault 1.3.0 to help the organization secure records stored in its intranet portal. However, the following security violations have been discovered:

- Vault server has been deployed with its root and unseal keys saved in a plaintext file on local disk.
- Web application uses static database credentials written to a plaintext file on local disk.
- The database password credentials are never rotated.
- Web application is manually authenticating to Vault using a `root` token.

### Security Requirements

- Vault unseal keys must be stored and encrypted in a safe location.
- Database credentials must be stored encrypted in a safe location.
- Database password must be periodically rotated.
- Web application must authenticate with Vault automatically with its token.
- Web application must have read-only access to access database credentials from Vault.

## Challenges

You must perform the following tasks to conform with the observed security requirements:

1. Re-deploy Vault to store its unseal keys in AWS KMS.
2. Generate new token and unseal keys.
3. Set up the AWS auth method in the Vault Server
4. Configure Vault Agent to do AWS auto-auth and write a sink to the file that Vault is using.
5. Enable Database Secret Engine in Vault to work with RDS and create a new ACL policy.
6. Use Consul Template for the Web App to authenticate and pull secrets from Vault Server.

## Extra Challenges - Next Steps

1. Deploy a bastion host on a AWS EC2 public subnet, with SSH access from any network.
2. Set up Vault behind an internal AWS Load Balancer with SSL encryption on a AWS EC2 private subnet.
3. Set up the Web Application behind a public Load Balancer with SSL encryption.


## Step 1: Re-deploy Vault to store its unseal keys in AWS KMS (auto-unseal)

> This step must be performed on the Vault **Server** and terminal.

> **NOTE:** Before starting this step, please clone the `vault-workshop` repo, `cd` into it, then deploy the workshop stack via `terraform`.

In this step, you will verify that the Vault has been deployed with its unseal keys and root token in plaintext on local disk.
To remediate this bad deployment, you will re-deploy Vault server and set up KMS to encrypt and unseal keys.

1.  Copy the values of the Terraform terminal output from the server Vault provisioning to a safe place. Take a moment to visit the `Vault_Server_HTTP_Address` and the `Web_Server_HTTP_Address` endpoint in your Web browser.

    ```sh
    RDS_MySQL_Host = petclinic.c3fnkh3bwk3i.us-west-2.rds.amazonaws.com
    RDS_MySQL_Port = 3306
    RDS_MySql_Url = jdbc:mysql://petclinic.c3fnkh3bwk3i.us-west-2.rds.amazonaws.com:3306/petclinic
    RDS_MySQL_DB_Name = petclinic
    Vault_Server_HTTP_Address = http://35.164.187.148:8200
    Vault_Server_Public_IP = 35.164.187.148
    Web_Server_Public_IP = 34.219.68.126
    Web_Server_HTTP_Address = http://34.219.68.126:8080
    ```

1.  SSH into the Vault **Server** instance.

    ```sh
    ssh -i <path_to_key> ubuntu@<public_ip_of_server>
    ...
    Are you sure you want to continue connecting (yes/no)? yes
    ```

    When you are prompted, enter "yes" to continue.

1.  To verify that Vault has been initialized and unsealed, run `vault status` command and notice that **Initialized** is `true`. 

    ```sh
    $ vault status
    Key                      Value
    ---                      -----
    Recovery Seal Type       shamir
    Initialized              true
    Sealed                   false
    Total Recovery Shares    1
    Threshold                1
    Version                  1.3.0
    Cluster Name             vault-cluster-d22f3407
    Cluster ID               b94acd28-2230-dda9-df9f-bf4a3f47f79c
    HA Enabled               false  
    ```

1.  Vault server has already been initialized for you and its unseal and root keys have been saved into local disk at path `/etc/vault/vault-init.txt`.       Verify this by running:

    ```sh
    $ cat /etc/vault/vault-init.txt
    Unseal Key 1: wPdYf7IJJspqew5LAyrM8SkRMF72R/Pqwd8eNRAqYok=

    Initial Root Token: s.n66t2JV6dWmNKVIcmPgsEX1f

    Vault initialized with 1 key shares and a key threshold of 1. Please securely
    distribute the key shares printed above. When the Vault is re-sealed,
    restarted, or stopped, you must supply at least 1 of these keys to unseal it
    before it can start servicing requests.

    Vault does not store the generated master key. Without at least 1 key to
    reconstruct the master key, Vault will remain permanently sealed!

    It is possible to generate new unseal keys, provided you have a quorum of
    existing unseal keys shares. See "vault operator rekey" for more information.

    ```

    > The following must be performed on at your Cloud 9 instance.

1.  Re-deploy Vault with a new configuration to use AWS KMS to encrypt its unseal keys. 

    Remove lines `#66-#77` and `#100-#131` (`Initialize Vault - Token in Clear txt` code block)
    Add the code block below under line `#65` in the [vault server template file](https://gitlab.com/ibuchh/reinvent-ecs-vault/blob/master/vault-workshop/templates/vault-server.tpl)
    Save all changes in file `vault-server.tpl`.

    ```sh
    cat << EOF | sudo tee /etc/vault.d/vault.hcl
    storage "file" {
      path = "/opt/vault"
    }
    listener "tcp" {
      address     = "0.0.0.0:8200"
      tls_disable = 1
    }
    seal "awskms" {
      region     = "${aws_region}"
      kms_key_id = "${kms_key}"
    }
    ui=true
    disable_mlock = true
    EOF
    ```

    Uncomment lines `#93-#105` and line `#88` in [petclinic.tf](https://gitlab.com/ibuchh/reinvent-ecs-vault/blob/master/vault-workshop/petclinic.tf).
    Save all changes in file `petclinic.tf`.

    ```sh
    #### Step 1, item 5: Set up Vault to use AWS KMS ####
    resource "aws_kms_key" "vault_unseal" {
      description             = "Vault unseal key"
      deletion_window_in_days = 10

      tags = {
        Name = "vault-kms-unseal-${var.stack}-vault-server"
      }
    }

    resource "aws_kms_alias" "vault_alias" {
      name          = "alias/vault-kms-unseal-${var.stack}-vault-server"
      target_key_id = aws_kms_key.vault_unseal.key_id
    }
    ```

    Run `terraform plan` and `terraform apply`. Confirm changes then redeploy.


## Step 2: Generate new token and unseal keys

> This step must be performed on at your terminal.

In this step, your generate new unseal keys and root token

1.  SSH into the Vault **Server** instance.

    ```sh
    ssh -i <path_to_key> ubuntu@<public_ip_of_server>
    ...
    Are you sure you want to continue connecting (yes/no)? yes
    ```
1.  Generate 5 new unseal keys and set 3 minimum to unsel/seal. Copy all 5 unseal keys and root token to a safe location

    ```sh
    $ vault operator init -recovery-shares=5 -recovery-threshold=3
    Recovery Key 1: ukJd9Wdna4XEb3AexoXqsrYzKX2fUp1PqUGa5JbJsjzR
    Recovery Key 2: nXpiZyA7NIO1LV6N8PuCHdCim8m8+kGMgbBRtCe1WBVt
    Recovery Key 3: FECM6FUwhcIWSbgZU1yoHi1plXSDVlnyd79Q4fBWB2mi
    Recovery Key 4: EOX/0iFr2VHrMUkelhDQmAK135K+sSCDddLHJa3c2Hnj
    Recovery Key 5: L9ny8oPMCANEH0yWz9B78KggXFDSvWfmcN5ti7aObynb

    Initial Root Token: s.Tkc6q8WJ1kb9cY1Yad0vSrVX

    Success! Vault is initialized

    Recovery key initialized with 5 key shares and a key threshold of 3. Please
    securely distribute the key shares printed above.
    ```

## Step 3: Set up the AWS auth method in Vault Server

> This step must be performed on the Vault **Server**.

In this step, you will configure Vault to allow AWS IAM authentication from the client Web server to Vault.

1.  SSH into the Vault **Server** instance.

    ```sh
    ssh -i <path_to_key> ubuntu@<public_ip_of_server>
    ...
    Are you sure you want to continue connecting (yes/no)? yes
    ```

    When you are prompted, enter "yes" to continue.

1.  To verify that Vault has been installed, run `vault status` command and notice that **Initialized** is `true`. Vault server has already been initialized for you.

    ```sh
    $ vault status
    Key                      Value
    ---                      -----
    Recovery Seal Type       shamir
    Initialized              true
    Sealed                   false
    Total Recovery Shares    5
    Threshold                3
    Version                  1.3.0
    Cluster Name             vault-cluster-7c1c8017
    Cluster ID               163a04b9-40da-8f76-3531-cf149d2ca585
    HA Enabled               false
    ```

1.  Log into Vault using the generated initial root token provided by the previous step:

    ```sh
    $ vault login s.Tkc6q8WJ1kb9cY1Yad0vSrVX
    Success! You are now authenticated. The token information displayed below
    is already stored in the token helper. You do NOT need to run "vault login"
    again. Future Vault requests will automatically use this token.

    Key                  Value
    ---                  -----
    token                s.Tkc6q8WJ1kb9cY1Yad0vSrVX
    token_accessor       BYXCBphDOFlouLAJDlDRvmoV
    token_duration       ∞
    token_renewable      false
    token_policies       ["root"]
    identity_policies    []
    policies             ["root"]
    ```

1.  Enable the `aws` auth method. This will enable the Web Server instance to authenticate to Vault via its associated EC2 IAM role. Those policies
    have been defined in the `iam.tf` file:

    ```sh
    $ vault auth enable aws
    Success! Enabled aws auth method at: aws/
    ```

1.  Configure the AWS credentials that Vault will use to verify login requests
    from AWS clients:

    ```sh
    $ vault write -force auth/aws/config/client
    ```

    > **NOTE:** In this example, it is expected that the EC2 instance profile of the client machine provides its credentials to Vault. See [AWS Auth Method documentation](https://www.vaultproject.io./docs/auth/aws.html#recommended-vault-iam-policy)
    for more details.

1.  Write some secrets at `secret/GameDay/database/`, and create `gamedayapp` policy which
    grants read-only permission on the `secret/data/GameDay/*` path:

    ```sh
    # Create a policy file
    $ tee gamedayapp.hcl <<EOF
    path "secret/data/GameDay/*" {
        capabilities = ["read", "list"]
    }
    EOF

    # Create a policy named, 'gamedayapp'
    $ vault policy write gamedayapp gamedayapp.hcl
    Success! Uploaded policy: gamedayapp

    # Enable K/V secrets version 2 engine at 'secret/'
    $ vault secrets enable -version=2 -path="secret" kv

    # Write the provided database secrets in 'kv/GameDay/database' path
    $ vault kv put secret/GameDay/database \
        dbname='petclinic' \
        dbuser='root' \
        dbpassword='petclinic'
    Key              Value
    ---              -----
    created_time     2019-11-27T05:15:29.224218418Z
    deletion_time    n/a
    destroyed        false
    version          1
    ```
1.  Log in to the [Amazon EC2 Dashboard](https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2), and select **Instances**. In [Step 1][step-1], Terraform provisioned the Web server instance (`"${var.stack}-web-server"`). Select the client instance, and click on its IAM role name.

    ![Secure Introduction](https://s3.amazonaws.com/ee-assets-prod-us-east-1/modules/1832097c35f54f1b8212fb1efeb92e07/v1/vault-client-01.png)

1.  Copy the **Role ARN** of the Vault client instance.

    ![Secure Introduction](https://s3.amazonaws.com/ee-assets-prod-us-east-1/modules/1832097c35f54f1b8212fb1efeb92e07/v1/vault-client-02.png)

1.  In the Vault **server** SSH session, execute the following command to configure a client role where `<ROLE_ARN>` is the IAM role ARN you copied.

    ```sh
    $ vault write auth/aws/role/client-role-iam auth_type=iam \
            bound_iam_principal_arn=<ROLE_ARN> \
            policies=gamedayapp \
            ttl=24h

    Success! Data written to: auth/aws/role/client-role-iam
    ```

    This creates `client-role-iam` role which has `gamedayapp` policy attached.

## Step 4: Configure Vault Agent  and Leverage Vault Agent Auto-Auth

> This step should be performed on the Vault **Client** instance.

Now that you have configured the appropriate AWS IAM auth method on our Vault server, let's SSH into the **client** instance and verify that you can utilize the instance profile to login to Vault.

1.  Log into Vault using the AWS auth method:

    ```sh
    $ vault login -method=aws role=client-role-iam

    Success! You are now authenticated. The token information displayed below
    is already stored in the token helper. You do NOT need to run "vault login"
    again. Future Vault requests will automatically use this token.

    Key                                Value
    ---                                -----
    token                              s.KdIVRaYH4DxVSOm4dhK066iO
    token_accessor                     DS1yFsFebyJVamOv357KTUSw
    token_duration                     24h
    token_renewable                    true
    token_policies                     ["default" "gamedayapp"]
    identity_policies                  []
    policies                           ["default" "gamedayapp"]
    token_meta_auth_type               iam
    token_meta_canonical_arn           arn:aws:iam::AWSACCOUNTNUMBER:role/GameDay-vault-client-role
    token_meta_client_arn              arn:aws:sts::AWSACCOUNTNUMBER:assumed-role/GameDay-vault-client-role/i-01b7d60c51565f1b9
    token_meta_inferred_aws_region     n/a
    token_meta_role_id                 3f5d32e9-8ded-110d-91ce-a85fa2f414ff
    token_meta_account_id              AWSACCOUNTNUMBER
    token_meta_client_user_id          AROA3SOPXIBPCHFYZ7HK3
    token_meta_inferred_entity_id      n/a
    token_meta_inferred_entity_type    n/a
    ```

1.  Check to make sure that the token has the appropriate permissions to read the secrets:

    ```sh
    $ vault kv get secret/GameDay/database
    ====== Metadata ======
    Key              Value
    ---              -----
    created_time     2019-11-27T05:37:34.404645275Z
    deletion_time    n/a
    destroyed        false
    version          1

    ======= Data =======
    Key           Value
    ---           -----
    dbname        petclinic
    dbpassword    petclinic
    dbuser        root
    ```
  > **NOTE:** The command rand above verifies that the Web server can read Vault's kv secrets engine via AWS auth.

Now, you are going to see how Vault Agent Auto-Auth method works, and write out a token to an arbitrary location on disk. Vault Agent is a **client daemon** and its Auto-Auth feature allows for easy authentication to Vault.

![Vault Agent](https://hashicorp-education.s3-us-west-2.amazonaws.com/ASWSGameDay/vault-secure-intro-5.png)


1.  In the **client** instance, explore the Vault Agent configuration file (`/home/ubuntu/vault-agent.hcl`).

    ```sh
    $ cat vault-agent.hcl
    exit_after_auth = true
    pid_file = "./pidfile"
    auto_auth {
      method "aws" {
          mount_path = "auth/aws"
          config = {
              type = "iam"
              role = "client-role-iam"
          }
      }
      sink "file" {
          config = {
              path = "/home/ubuntu/vault-token-via-agent"
          }
      }
    }
    vault {
      address = "http://172.17.2.24:8200"
    }
    ```

    > **NOTE:** The `vault` block points to the Vault server `address`. This should match to the private IP address of your Vault server host.

    The top level `auto_auth` block has two configuration entries: `method` and `sinks`. In this example, the Auto-Auth is configured to use the `aws` auth method enabled at the `auth/aws` path on the Vault **server**. The Vault Agent will use the `client-role-iam` role to authenticate.

    The `sink` block specifies the location on disk where to write tokens. Vault Agent Auto-Auth `sink` can be configured multiple times if you want Vault Agent to place the token into multiple locations. In this example, the `sink` is set to `/home/ubuntu/vault-token-via-agent`.

1.  Now you will need to set the agent to use Consul template to enable the agent to authenticate to Vault and provide the Web app with the                   necessary Dabatase credentials it needs to be able run. The Consul template has been saved in this path `/opt/spring-petclinic/src/main/resources/application-mysql.properties.tmpl`, while the Web app's database properties has been saved in this path `/opt/spring-petclinic/target/classes/application-mysql.properties` after it was compiled by `maven`.

    ```sh
    # Switch to root user
    $ sudo su
    # Verify that the Web app is reading its database credentials in plaintext
    $ cat /opt/spring-petclinic/src/main/resources/application-mysql.properties
    # database init, supports mysql too
    database=mysql
    # SQL is written to be idempotent so this is safe
    spring.datasource.initialization-mode=always
    # Datasource driver class
    spring.datasource.driver-class-name=com.mysql.jdbc.Driver
    # Spring profile to start with
    spring.profiles.active=mysql
    # Database Credentials
    # your local database username, just a user that has readwrite permissions
    spring.datasource.username=root
    # local database password
    spring.datasource.password=ech9Weith4Phei7W
    # this connection URL assumes that it is connecting to a schema called broadleaf
    spring.datasource.url=jdbc:mysql://petclinic.c3fnkh3bwk3i.us-west-2.rds.amazonaws.com/petclinic

    # Enable Consul template in the vault-agent.hcl configuration file
    $ tee vault-agent.hcl <<EOL
    template {
       source      = "/opt/spring-petclinic/src/main/resources/application-mysql.properties.tmpl"
       destination = "/opt/spring-petclinic/src/main/resources/application-mysql.properties"
    }
    EOL
    ```

## Step 5: Enable Database Secrets Engine

> This step should be performed on the Vault **Server** instance.

In this step, you will set up the database secrets engine for mysql, and then configure it so that it can connect to AWS RDS Mysql server. You will also set up set up a static role that periodically rotates the database password.

This approach rotates the password only. The username remains the same.

1.  First, let's disable Vault's kv secrets engine. The Web app will use Vault's database secrets engine, NOT the kv's secrets engine.

    ```sh
    $ vault login s.Tkc6q8WJ1kb9cY1Yad0vSrVX
    Success! You are now authenticated. The token information displayed below
    is already stored in the token helper. You do NOT need to run "vault login"
    again. Future Vault requests will automatically use this token.

    Key                  Value
    ---                  -----
    token                s.bw9N7FFrMkh1IFGojl0nupzZ
    token_accessor       l4MYQolHEworjNvhkxvRTZhl
    token_duration       ∞
    token_renewable      false
    token_policies       ["root"]
    identity_policies    []
    policies             ["root"]

    $ vault secrets disable secret/
    Success! Disabled the secrets engine (if it existed) at: secret/
    ```

1.  Execute the following command to enable the database secrets engine at `database/` path.

    ```sh
    $ vault secrets enable database
    ```

1.  Execute the following command to configure the database secrets engine which uses `mysql-database-plugin`. Replace the `<DB_HOST URL>` string with the address you copied from `RDS_MySQL_Host` terraform outputs in `Step 1`.

    ```sh
    $ vault write database/config/mysql \
            plugin_name=mysql-database-plugin \
            allowed_roles="rotate-mysql-pass" \
            connection_url="{{username}}:{{password}}@tcp(<DB_HOST URL>:3306)/" \
            username="root" \
            password="ech9Weith4Phei7W"
    ```

1.  Execute the following command to create a static role, `rotate-mysql-pass`.

    ```sh
    $ vault write database/static-roles/rotate-mysql-pass \
            db_name=mysql \
            rotation_statements="ALTER USER "{{name}}" IDENTIFIED BY '{{password}}';" \
            username="root" \
            rotation_period=600
    ```

> **NOTE:** For static roles, the `db_name` parameter is the database configuration name (_not_ the database name). In this scenario, you configured `database/config/mysql`; therefore, the `db_name` must be set to `mysql`. The above command creates a `rotate-mysql-pass` static role with database username `root` whose password gets rotated every 600 seconds.


1.  To verify, execute to following command to read back the `rotate-mysql-pass` role definition:

    ```sh
    $ vault read database/static-roles/rotate-mysql-pass
    Key                    Value
    ---                    -----
    db_name                mysql
    last_vault_rotation    2019-11-27T07:16:45.876377404Z
    rotation_period        600s
    rotation_statements    [ALTER USER {{name}} IDENTIFIED BY '{{password}}';]
    username               root
    ```

1.  Now that we have set up a static role to rotate RDS MySQL credentials, let's update the Web App's current HCL policy named `gamedayapp` in Vault server:

    ```sh
    # Add the new capabilities into the policy definition
    $ tee gamedayapp.hcl <<EOF
    path "database/static-creds/rotate-mysql-pass" {
      capabilities = ["read"]
    }
    EOF

    # Update the policy named 'gamedayapp'
    $ vault policy write gamedayapp gamedayapp.hcl
    Success! Uploaded policy: gamedayapp
    ```

## Step 6: Use Consul Template for the Web App authenticate via Vault agent

> This step should be performed on the Vault **Client** instance.

In this step, you will configure Consul Template syntax within the Web App's database credentials

1.  Add the Consul Template syntax in the Web App's Consul template. Replace the `<RDS_MySQL_Host>` and `<RDS_MySQL_DB_Name>` strings with their corresponding values shown in the `terraform` outputs in `Step 1`:

    ```sh
    # Add the new consul template syntax into the template file
    $ tee /opt/spring-petclinic/src/main/resources/application-mysql.properties.tmpl <<EOL
    {{ with secret "database/static-creds/rotate-mysql-pass" }}
    spring.datasource.username={{ .Data.username }}
    spring.datasource.password={{ .Data.password }}
    {{ end }}
    spring.datasource.url=jdbc:mysql://<RDS_MySQL_Host>/<RDS_MySQL_DB_Name>
    EOL
    ```
1.  Start Vault agent and restart the Web app service:

    ```sh
    # Start the Vault agent in /home/ubuntu
    $ vault agent -config=vault-agent.hcl -log-level=debug

    # Restart the Web Server
    $ pkill java
    $ cd /opt/spring-petclinic && SPRING_PROFILES_ACTIVE=mysql /usr/bin/mvn spring-boot:run
    ```

1.  Insert the `Web_Server_HTTP_Address` URL into your browser and hit ENTER. You should see the Web app up and running again. Log into the Vault server using your root token and navigate throught the configurations made in Vault.