# ---------------------------------------------------------------------------------------------------------------------
# Vault Server Outputs
# ---------------------------------------------------------------------------------------------------------------------
output "Vault_Server_HTTP_Address" {
  value = "http://${aws_instance.vault-server.public_ip}:8200"
}

output "Vault_Server_Public_IP" {
  value = "${aws_instance.vault-server.public_ip}"
}


# ---------------------------------------------------------------------------------------------------------------------
# PetClinic Web Server Output
# ---------------------------------------------------------------------------------------------------------------------
output "Web_Server_Public_IP" {
  value = "${aws_instance.website.public_ip}"
}
output "Web_Server_HTTP_Address" {
  value = "http://${aws_instance.website.public_ip}:8080"
}


# ---------------------------------------------------------------------------------------------------------------------
# RDS MySql Outputs
# ---------------------------------------------------------------------------------------------------------------------
output "RDS_MySql_Url" {
  value = "jdbc:mysql://${aws_db_instance.db.address}/${var.db_name}"
}

output "RDS_MySQL_Host" {
  value = "${aws_db_instance.db.address}"
}

output "RDS_MySQL_Port" {
  value = "${aws_db_instance.db.port}"
}

output "RDS_MySQL_DB_Name" {
  value = "${var.db_name}"
}


