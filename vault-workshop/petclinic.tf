# ---------------------------------------------------------------------------------------------------------------------
# DATA FOR AMI and INSTANCE PROFILE
# ---------------------------------------------------------------------------------------------------------------------
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*aws_gameday_petclinic_app_ubuntu_16.04*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["795541454942"] # Canonical
}

data "aws_iam_instance_profile" "vault-stack" {
  name = "TeamRoleInstanceProfile"
}


# ---------------------------------------------------------------------------------------------------------------------
# RDS (MYSQL)
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_db_subnet_group" "db-subnet-grp" {
  name        = "petclinic-db-sgrp"
  description = "Database Subnet Group"
  subnet_ids  = aws_subnet.private.*.id
}

resource "aws_db_instance" "db" {
  identifier             = "petclinic"
  allocated_storage      = 5
  engine                 = "mysql"
  engine_version         = "5.7"
  port                   = "3306"
  instance_class         = var.db_instance_type
  name                   = var.db_name
  username               = var.db_user
  password               = var.db_password
  availability_zone      = "${var.aws_region}a"
  vpc_security_group_ids = [aws_security_group.db-sg.id]
  multi_az               = false
  db_subnet_group_name   = aws_db_subnet_group.db-subnet-grp.id
  parameter_group_name   = "default.mysql5.7"
  publicly_accessible    = false
  skip_final_snapshot    = true

  tags = {
    Name = "${var.stack}-db"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# VAULT SERVER INSTANCE
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_instance" "vault-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.client_instance_type
  user_data                   = data.template_file.setup-vault.rendered
  subnet_id                   = aws_subnet.public[1].id
  key_name                    = var.keyPairName
  vpc_security_group_ids      = [aws_security_group.vault-server_sg.id]
  associate_public_ip_address = true
  iam_instance_profile        = data.aws_iam_instance_profile.vault-stack.name
  depends_on                  = [aws_db_instance.db]

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = "60"
  }
  tags = {
    Name    = "${var.stack}-vault-server"
    Project = var.stack
  }
}

data "template_file" "setup-vault" {
  template = file("${path.module}/templates/vault-server.tpl")

  vars = {
    aws_region = var.aws_region
    # kms_key    = aws_kms_key.vault_unseal.id
  }
}

# #### Step 1, item 5: Set up Vault to use AWS KMS ####
# resource "aws_kms_key" "vault_unseal" {
#   description             = "Vault unseal key"
#   deletion_window_in_days = 10

#   tags = {
#     Name = "vault-kms-unseal-${var.stack}-vault-server"
#   }
# }

# resource "aws_kms_alias" "vault_alias" {
#   name          = "alias/vault-kms-unseal-${var.stack}-vault-server"
#   target_key_id = aws_kms_key.vault_unseal.key_id
# }

# ---------------------------------------------------------------------------------------------------------------------
# WEBSITE INSTANCE
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_instance" "website" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.client_instance_type
  user_data                   = data.template_file.website.rendered
  subnet_id                   = aws_subnet.public[0].id
  key_name                    = var.keyPairName
  vpc_security_group_ids      = [aws_security_group.vault-client_sg.id]
  associate_public_ip_address = true
  iam_instance_profile        = data.aws_iam_instance_profile.vault-stack.name
  depends_on                  = [aws_db_instance.db, aws_instance.vault-server]

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = "60"
  }
  tags = {
    Name    = "${var.stack}-web-server"
    Project = var.stack
  }
}

data "template_file" "website" {
  template = file("${path.module}/templates/petclinic-app.tpl")

  vars = {
    vault_server_addr  = aws_instance.vault-server.private_ip
    rds_mysql_endpoint = aws_db_instance.db.address
    db_name            = var.db_name
    db_user            = var.db_user
    db_password        = var.db_password
  }
}
