terraform {
  backend "remote" {
    organization = "ORGANIZATION_NAME"
    hostname     = "app.terraform.io"

    workspaces {
      name = "WORKSPACE_NAME"
    }
  }

  required_providers {
    aws = "~> 2.25"
  }
}
