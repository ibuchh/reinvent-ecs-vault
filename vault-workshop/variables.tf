variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default     = "172.17.0.0/16"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "keyPairName" {
  description = "Name of the pem key to use for SSH"
  default     = "ee-default-keypair"
}

variable "client_instance_type" {
  description = "Client instance type"
  default     = "t2.micro"
}

variable "stack" {
  description = "Name of the stack."
  default     = "GameDay"
}

variable "db_instance_type" {
  description = "RDS instance type"
  default     = "db.m4.large"
}

variable "db_name" {
  description = "RDS DB name"
  default     = "petclinic"
}

variable "db_user" {
  description = "RDS DB username"
  default     = "root"
}

variable "db_password" {
  description = "RDS DB password"
  default     = "ech9Weith4Phei7W"
}
variable "db_profile" {
  description = "RDS Profile"
  default     = "mysql"
}
variable "db_initialize" {
  description = "RDS initialize"
  default     = "yes"
}

